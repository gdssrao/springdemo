package com.spring.demo;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

@RequestMapping(value="/", method=RequestMethod.GET)
public String gethelloworld(@RequestParam(value="name",defaultValue="World") String name) {
	return "Hello "+name;
}

@RequestMapping(value="/", method=RequestMethod.POST)
public String posthelloworld(@RequestBody String name)
{
	return "Hai " + name;
}

@RequestMapping(value="/", method=RequestMethod.PUT)

public String puthelloworld(@RequestBody String name) {
	return "PUT Rest Service successfull";
}

@RequestMapping(value="/", method=RequestMethod.DELETE)
public String deletehelloworld(@RequestBody String name) {
	return "DELETE Rest Service Successfull";
}
}

